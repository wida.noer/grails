package grails.rest


import grails.rest.*
import grails.converters.*

class BookController extends RestfulController {
    static responseFormats = ['json', 'xml']
    BookController() {
        super(Book)
    }
}
