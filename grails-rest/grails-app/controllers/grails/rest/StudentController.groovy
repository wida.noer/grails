package grails.rest


import grails.rest.*
import grails.converters.*

class StudentController extends RestfulController<Student> {

    static responseFormats = ['json', 'xml']
    StudentController() {
        super(Student)
    }

    StudentService studentService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index() {
        //respond ([studentService.list()], status: 200)
        respond studentService.list()
    }

    def show(Long id) {
        respond studentService.get(id)
    }

    def create() {
        respond new Student(params)
    }

    def save(Student student) {
        studentService.save(student)
        render "Student Saved", status: 201
//        redirect action:"index", method:"GET"
    }

    def delete(Long id) {
        studentService.delete(id)
        render "Student Deleted", status: 200
//        redirect action:"index", method:"GET"
    }

    def findByGender(String gender) {
        respond studentService.getByGender(gender)
    }

    def getAge(int age) {
        respond studentService.getAge(age)
    }

}
