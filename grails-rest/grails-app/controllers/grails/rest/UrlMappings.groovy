package grails.rest

class UrlMappings {

    static mappings = {

        // Controller City
        "/cities"(controller: "city", action: "index", method:"GET")

        "/cities/$id"(controller: "city", action: "show", method:"GET")

        "/cities"(controller: "city", action: "save", method:"POST")
        "/cities/$id"(controller: "city", action: "update", method:"PUT")

        "/cities/$id"(controller: "city", action: "delete", method:"DELETE")

        // Controller Location
        "/cities/$cityId/locations"(controller: "location", action: "index", method:"GET")
        "/cities/$cityId/locations/$id"(controller: "location", action: "show", method:"GET")

        "/cities/$cityId/locations"(controller: "location", action: "save", method:"POST")
        "/cities/$cityId/locations/$id"(controller: "location", action: "update", method:"PUT")

        "/cities/$cityId/locations/$id"(controller: "location", action: "delete", method:"DELETE")

        "/"(controller: 'application', action:'index')

        // Controller Student
        "/student"(controller: "student", action: "index", method:"GET")

        "/student/$id"(controller: "student", action: "show", method:"GET")

        "/student"(controller: "student", action: "save", method:"POST")
        "/student/$id"(controller: "student", action: "update", method:"PUT")

        "/student/$id"(controller: "student", action: "delete", method:"DELETE")

        "/student/$gender"(controller: "student", action: "findByGender", method:"GET")

        "/student/$age"(controller: "student", action: "getAge", method:"GET")

        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
        "500"(view:'/error')
        "404"(view:'/notFound')
        "401"(view:'/unauthorized')
        "403"(view:'/forbidden')
        "400"(view:'/badrequest')
        // "/student"(resources:'student')
        "/article"(resources:'article')
        "/category"(resources:'category')
    }
}
