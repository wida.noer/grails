package grails.rest


import grails.rest.*
import grails.converters.*

class AuthorController extends RestfulController {
    static responseFormats = ['json', 'xml']
    AuthorController() {
        super(Author)
    }
}
