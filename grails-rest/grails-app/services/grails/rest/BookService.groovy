package grails.rest

import grails.gorm.transactions.ReadOnly
import grails.gorm.transactions.Transactional

@Transactional
class BookService {

    @ReadOnly('books')
    List<Book> findAll() {
        Book.where {}.findAll()
    }

    def serviceMethod() {

    }
}
