package grails.rest

class Book {

    String title

    static belongTo = [author: Author]

    static constraints = {
    }

}
