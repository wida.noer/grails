package grails.rest

import grails.rest.Resource

//@Resource(uri='/student', formats=['json', 'xml'])
class Student {

    String name
    String gender
    int age
    String address

    static constraints = {
        name blank: false
        gender blank: false
        age blank: false
        address blank: false
    }
}
