package grails.rest

import grails.rest.Resource


@Resource(uri='/article', formats=['json', 'xml'])
class Article {

    String title
    String author
    String description
    String content
    Date createDate =  new Date()
    static belongTo = [category: Category]

    static constraints = {
        title blank: false
        author blank: false
        description blank: false
        content blank: false
    }
}
