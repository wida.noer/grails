package grails.rest

import grails.rest.Resource

@Resource(uri='/category', formats=['json', 'xml'])
class Category {

    String catName
    static hasMany = [articles: Article]

    static constraints = {
        catName blank: false
    }
}
