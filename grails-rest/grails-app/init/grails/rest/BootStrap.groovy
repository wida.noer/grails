package grails.rest

//import grails.converters.JSON
//import java.text.SimpleDateFormat

class BootStrap {

    def init = { servletContext ->
        //server = Server.createTcpServer(args).start()
        new Student(name: "Wida Nur Hasan", gender: "Male", age: 25, address: "Purwokerto").save()
        new Student(name: "Angga Wardana", gender: "Male", age: 24, address: "Yogyakarta").save()

        def fictBook = new Book(title: "IT")
        def nonFictBook = new Book(title: "On Writing: A Memoir of the Craft")
        def a = new Author(name: "Stephen King")
                .addToFiction(fictBook)
                .addToNonFiction(nonFictBook)
                .save()

//        Student student = new Student()
//        student.address = "Bendungan Hilir"

//        def category = new Category(catName:"Programming")
//        def article1 = new Article(title: "Mengenal Java Spring Boot", author: "Wida Nur Hasan", description: "Tutorial dasar step by step Java Spring Boot", content: "Tutorial dasar step by step Java Spring Boot")
//        def article2 = new Article(title: "Grails Framework", author: "Ahmad Maulana", description: "Tutorial step by step Grails", content: "Tutorial dasar step by step Grails")
//
//        def category_article1 = category.addToArticles(article1).save()
//        def category_article2 = category.addToArticles(article2).save()

        def category = new Category(catName:"Programming")
                .addToArticles(new Article(title:"Spring Boot",author:"Wida Nur Hasan.",description:"Step by step tutorial Spring Boot",content:"Step by step tutorial Spring Boot"))
                .save()
                .addToArticles(new Article(title:"The Grails Framework",author:"Angga Maulana.",description:"Step by step tutorial grails",content:"Step by step tutorial grails"))
                .save()

        if (City.count() < 1) {
            def city1 = new City(name: 'Dhaka', division: 'Dhaka', description: 'Capital of BD', population: 15000000)
            def city2 = new City(name: 'Tangail', division: 'Dhaka', description: 'A District of BD', population: 500000)

            def loc1 = new Location(streetAddress: '23 Gulshan', streetAddressLine2: 'lane 13', postalCode: '1212', stateProvince: 'Dhaka', lat: 23.789707, lon: 90.392924)
            def loc2 = new Location(streetAddress: '12 Badda', streetAddressLine2: 'lane 11', postalCode: '1212', stateProvince: 'Dhaka', lat: 23.778280, lon: 90.424983)
            def loc3 = new Location(streetAddress: '100 Uttara', streetAddressLine2: 'lane 21', postalCode: '1229', stateProvince: 'Dhaka', lat: 23.863396, lon: 90.398361)

            def loc4 = new Location(streetAddress: '23 Dhanbari', streetAddressLine2: 'lane 31', postalCode: '1997', stateProvince: 'Tangail', lat: 24.680874, lon: 89.960024)
            def loc5 = new Location(streetAddress: '100 Madupur', streetAddressLine2: 'lane 12', postalCode: '1990', stateProvince: 'Tangail', lat: 24.605670, lon: 90.032121)

            def d1 = city1.addToLocations(loc1).save()
            def d2 = city1.addToLocations(loc2).save()
            def d3 = city1.addToLocations(loc3).save()
            def d4 = city2.addToLocations(loc4).save()
            def d5 = city2.addToLocations(loc5).save()

        }

        def destroy = {
            //server.stop()
        }
    }

//    private void articleMarshaler() {
//        JSON.registerObjectMarshaller(Category) { cat ->
//            [
//                    id: cat.id,
//                    name: cat.catName,
//                    article: cat.articles.collect { article ->
//                        [
//                                id: article.id,
//                                title: article.title,
//                                author: article.author,
//                                description: article.description,
//                                content: article.content,
//                                created: new SimpleDateFormat("dd/MM/yyyy").format(article.createDate)
//                        ]
//                    }
//            ]
//        }
//    }

}
